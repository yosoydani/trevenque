
## Matrigest

El sistema para la gestión de matriculas para la universidad de Granada.

El sistema esta desarrollado entermente en Laravel. Se ha usado el sistema de contenedores propio de Laravel llamado sail.

### Iniciar el sistema
Si no existe copiar el archivo .env.example a .env

Ejecutar los siguientes comandos:

vendor/bin/sail up -d

vendor/bin/sail composer install

vendor/bin/sail migrate

vendor/bin/sail db:seed


Navegar a localhost.

## Consideraciones
Debido al poco tiempo disponible he simplificado algunas cosas.

No hay registro. Para hacerlo habria que asegurar las rutas y cambiar los test para que tengan acceso.

No  hay ni arquitectura hexagonal ni DDD, ni CQRS.
Primero porque es una aplicación sencilla y no es necesario.
Segundo, poco tiempo, me aprovecho de las ventajas de Laravel.

En la medida de lo posible he hecho TDD.

No se si me dará tiempo a terminarlo pero haré lo que pueda.



