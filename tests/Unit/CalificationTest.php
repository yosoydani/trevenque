<?php

namespace Tests\Unit;

use App\Models\Calification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CalificationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_calification_belong_to_a_subject()
    {
       $calification = Calification::factory()->create();
       $this->assertInstanceOf('App\Models\Subject',$calification->subject);
    }

    /** @test */
    public function a_calification_belong_to_a_student()
    {
        $calification = Calification::factory()->create();
        $this->assertInstanceOf('App\Models\Student',$calification->student);
    }


}
