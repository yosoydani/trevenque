<?php

namespace Tests\Unit;

use App\Models\Calification;
use App\Models\Student;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_student_has_many_tuitions()
    {
        $student = Student::factory()
            ->has(Tuition::factory()->count(3))
            ->create();

        $this->assertInstanceOf('App\Models\Tuition', $student->tuitions->first());
    }

    /** @test */
    public function a_student_has_many_califications()
    {
        $student = Student::factory()
            ->has(Calification::factory()->count(3))
            ->create();

        $this->assertInstanceOf('App\Models\Calification', $student->califications->first());
    }
}
