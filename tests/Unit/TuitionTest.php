<?php

namespace Tests\Unit;

use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TuitionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_tuition_belong_to_a_degree()
    {
        $tuition = Tuition::factory()->create();

       $this->assertInstanceOf('App\Models\Degree', $tuition->degree);
    }

    /** @test */
    public function a_tuition_belong_to_a_student()
    {
        $tuition = Tuition::factory()->create();

        $this->assertInstanceOf('App\Models\Student', $tuition->student);
    }

    /** @test */
    public function a_tuition_has_many_subject()
    {
        $tuition = Tuition::factory()
            ->has(Subject::factory()->count(5))
            ->create();

        $this->assertInstanceOf('App\Models\Subject', $tuition->subjects->first());
    }
}
