<?php

namespace Tests\Unit;

use App\Models\Calification;
use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SubjectTest extends TestCase
{
    use RefreshDatabase;

    /** @test  */
    public function a_subject_belong_to_a_degree()
    {
        $subject = Subject::factory()->create();
        $this->assertInstanceOf('App\Models\Degree', $subject->degree);
    }

    /** @test */
    public function a_subject_belong_many_tuitions()
    {
        $subject = Subject::factory()
            ->has(Tuition::factory()->count(5))
            ->create();

        $this->assertInstanceOf('App\Models\Tuition', $subject->tuitions->first());
    }

    /** @test */
    public function a_subject_has_many_cualifications()
    {
        $subject = Subject::factory()
            ->has(Calification::factory()->count(5))
            ->create();

        $this->assertInstanceOf('App\Models\Calification', $subject->califications->first());
    }



}
