<?php

namespace Tests\Unit;

use App\Models\Degree;
use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DegreeTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_degree_has_subjects()
    {
        $degree = Degree::factory()
            ->has(Subject::factory()->count(5))
            ->create();

        $this->assertInstanceOf('App\Models\Subject',$degree->subjects->first());
    }

    /** @test */
    public function a_degree_has_many_tuitions()
    {
        $degree = Degree::factory()
            ->has(Tuition::factory()->count(5))
            ->create();

        $this->assertInstanceOf('App\Models\Tuition',$degree->tuitions->first());
    }
}
