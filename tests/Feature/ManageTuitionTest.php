<?php

namespace Tests\Feature;

use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ManageTuitionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_browser_to_manage_tuition_page()
    {
        $tuition = Tuition::factory()->create();
        $response = $this->get('/gestionar-matriculas/' . $tuition->id);
        $response->assertStatus(200);
        $response->assertSee([
            $tuition->student->name,
            $tuition->degree->name,
        ]);
    }

    /** @test */
    public function a_user_can_add_subject_to_tuition()
    {

        $tuition = Tuition::factory()->create();
        $subject = Subject::factory()->create([
            'degree_id' => $tuition->degree->id
        ]);
        $this->post($tuition->managePath(), [
            'subject_id' => $subject->id
        ]);
        $response = $this->get($tuition->managePath());
        $response->assertSee([
            $subject->name,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);
    }

    /** @test */
    public function a_student_can_not_add_a_full_subject()
    {
        $tuition = Tuition::factory()->create();
        $subject = Subject::factory()->create([
            'degree_id' => $tuition->degree->id,
            'max_students' => 1
        ]);
        $tuition->subjects()->attach($subject->id);
        $response = $this->post($tuition->managePath(), [
            'subject_id' => $subject->id
        ]);
        $response->assertSessionHasErrors('subject_id');
    }

    // TODO add validations

    /** @test  */
    public function a_user_can_extract_a_subject_from_a_tuition()
    {
        $tuition = Tuition::factory()->create();
        $subject = Subject::factory()->create([
            'degree_id' => $tuition->degree->id
        ]);
        $tuition->subjects()->attach($subject->id);
        $this->delete($tuition->managePath(),['subject_id' => $subject->id]);
        $response = $this->get($tuition->managePath());
        $this->assertDatabaseMissing('subject_tuition', [
            'subject_id' => $subject->id,
            'tuition_id' => $tuition->id
        ]);
    }

    // TODO control delete subjects when a Tuition is delete

}
