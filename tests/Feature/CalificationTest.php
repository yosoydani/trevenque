<?php

namespace Tests\Feature;

use App\Models\Calification;
use Faker\Core\Number;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CalificationTest extends TestCase
{
    use RefreshDatabase;
    use WithFaker;


    /** @test */
    public function a_user_can_see_an_not_found_message_if_no_califications()
    {
        $response = $this->get('/calificaciones');
        $response->assertStatus(200);
        $response->assertSee('No hay datos disponibles');
    }

    /** @test */
    public function a_user_can_view_all_califications()
    {
        $calification = Calification::factory()->create();
        $response = $this->get('/calificaciones');
        $response->assertStatus(200);
        $response->assertSee([
            $calification->id,
            $calification->subject->name,
            $calification->student->fullName(),
            $calification->first_calification,
            $calification->second_calification,
        ]);
    }

    /** @test */
    public function a_user_can_see_a_single_calification()
    {
        $calification = Calification::factory()->create();
        $response = $this->get('/calificaciones/' . $calification->id);
        $response->assertStatus(200);
        $response->assertSee([
            $calification->id,
            $calification->subject->name,
            $calification->student->fullName(),
            $calification->first_calification,
            $calification->second_calification,
        ]);
    }

    /** @test */
    public function a_user_can_navigate_to_create_calification_page()
    {
        $response = $this->get('/calificaciones/crear');
        $response->assertStatus(200);
        $response->assertSee([
            'Crear Calificación',
            'Alumno',
            'Asignatura',
            'Primera Convocatoria',
            'Segunda Convocatoria',
        ]);
    }

    /** @test */
    public function a_user_can_create_a_calification()
    {
        $calification = Calification::factory()->make();
        $this->post('calificaciones', $calification->toArray());
        $response = $this->get($calification->path());
        $response->assertSee([
            $calification->id,
            $calification->subject->name,
            $calification->student->fullName(),
            $calification->first_calification,
            $calification->second_calification,
        ]);
    }

    // TODO test validadtion

    /** @test */
    public function a_user_can_navigate_to_edit_calification_page()
    {
        $calification = Calification::factory()->create();
        $response = $this->get($calification->path() . '/editar');
        $response->assertStatus(200);
        $response->assertSee([
            $calification->id,
            $calification->subject->name,
            $calification->student->fullName(),
            $calification->first_calification,
            $calification->second_calification,
        ]);
    }

    /** @test */
    public function a_user_can_edit_a_single_calification()
    {
        $calification = Calification::factory()->create();
        $newFirstCalification = $this->faker->numberBetween(0, 10);
        $newSecondCalification = $this->faker->numberBetween(0, 10);
        $this->patch('calificaciones/' . $calification->id,
            [
                'student_id' => $calification->student_id,
                'subject_id' => $calification->subject_id,
                'first_calification' => $newFirstCalification,
                'second_calification' => $newSecondCalification,
            ]);
        $response = $this->get($calification->path());
        $response->assertSee([$newFirstCalification, $newSecondCalification]);
    }

    /** @test */
    public function a_user_can_delete_a_single_calification()
    {
        $calification = Calification::factory()->create();
        $this->delete('calificaciones/'.$calification->id);
        $this->assertDatabaseMissing('califications', [
            'id' => $calification->id
        ]);
    }

}
