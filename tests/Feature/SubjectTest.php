<?php

namespace Tests\Feature;

use App\Models\Subject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class SubjectTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_user_can_see_an_not_found_message_if_no_degrees(){
        $response = $this->get('/asignaturas');
        $response->assertStatus(200);
        $response->assertSee('No hay datos disponibles');
    }

    /** @test */
    public function a_user_can_view_all_subjects()
    {
        $subject = Subject::factory()->create();
        $response = $this->get('/asignaturas');
        $response->assertStatus(200);
        $response->assertSee([
            $subject->name,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);
    }

    /** @test  */
    public function a_user_can_see_a_single_subject()
    {
        $subject = Subject::factory()->create();
        $response = $this->get('/asignaturas/'. $subject->id);
        $response->assertStatus(200);
        $response->assertSee([
            $subject->name,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);
    }

    /** @test  */
    public function a_user_can_navigate_to_create_subject_page()
    {
        $response = $this->get('/asignaturas/crear');
        $response->assertStatus(200);
        $response->assertSee([
            'Crear Asignatura',
            'Nombre',
            'Créditos',
            'Máx. alumnos',
            'Año académico',
            'Titulación',
            'Enviar',
            'Reiniciar'
        ]);
    }

    /** @test */
        public function a_user_can_create_a_subject()
    {
        $subject = Subject::factory()->make();
        $this->post('asignaturas', $subject->toArray());
        $response = $this->get($subject->path());

        $response->assertSee([
            $subject->name,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);
    }

    /** @test  */
    public function a_subject_must_have_a_name()
    {
        $subject = Subject::factory()->make(['name' => null]);
        $response = $this->post('asignaturas', $subject->toArray() );
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function a_subject_must_have_credits()
    {
        $subject = Subject::factory()->make(['credits' => 0]);

        $response = $this->post('asignaturas', $subject->toArray());
        $response->assertSessionHasErrors('credits');
    }

    /** @test  */
    public function a_subject_must_have_max_students()
    {
        $subject = Subject::factory()->make(['max_students' => 0]);

        $response = $this->post('asignaturas', $subject->toArray());
        $response->assertSessionHasErrors('max_students');
    }

    /** @test  */
    public function a_subject_must_have_a_year_after_2020()
    {
        $subject = Subject::factory()->make(['academic_year' => 2000]);

        $response = $this->post('asignaturas', $subject->toArray());
        $response->assertSessionHasErrors('academic_year');
    }

    /** @test  */
    public function a_subject_must_have_a_year_before_2155()
    {
        $subject = Subject::factory()->make(['academic_year' => 3000]);

        $response = $this->post('asignaturas', $subject->toArray());
        $response->assertSessionHasErrors('academic_year');
    }

    /** @test  */
    public function a_subject_must_have_a_valid_degree_id()
    {
        $subject = Subject::factory()->make(['degree_id' => 0]);
        $response = $this->post('asignaturas',$subject->toArray() );
        $response->assertSessionHasErrors('degree_id');
    }

    /** @test  */
    public function a_subject_must_be_unique_for_a_degree()
    {
        $subject = Subject::factory()->create();
        $response = $this->post('asignaturas', $subject->toArray());
        $response->assertSessionHasErrors('degree_id');
    }

    /** @test  */
    public function a_user_can_navigate_to_edit_subject_page()
    {
        $subject = Subject::factory()->create();
        $response = $this->get($subject->path().'/editar');
        $response->assertStatus(200);
        $response->assertSee([
            $subject->name,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);;
    }

    /** @test */
    public function a_user_can_edit_a_single_subject()
    {
        $subject = Subject::factory()->create();
        $newName = Str::random(25);
        $subject->name = $newName;
        $this->patch('asignaturas/'.$subject->id, $subject->toArray() );
        $response = $this->get($subject->path());
        $response->assertSee([
            $newName,
            $subject->credits,
            $subject->max_students,
            $subject->academic_year,
            $subject->degree->name
        ]);;;
    }

    /** @test */
    public function a_user_can_delete_a_single_subject()
    {
        $subject = Subject::factory()->create();
        $this->delete('asignaturas/'.$subject->id);
        $this->assertDatabaseMissing('subjects', [
            'id' => $subject->id
        ]);
    }

    // TODO borrar calificaciones si se borra la asignatura

}
