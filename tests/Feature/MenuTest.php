<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MenuTest extends TestCase
{
    /** @test */
    public function a_user_can_see_the_menu()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Titulaciones');
        $response->assertSee('Matrículas');
        $response->assertSee('Asignaturas');
        $response->assertSee('Alumnos');
    }
}
