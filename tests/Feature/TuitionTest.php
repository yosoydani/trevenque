<?php

namespace Tests\Feature;

use App\Models\Degree;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TuitionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_see_an_not_found_message_if_no_tuitions(){
        $response = $this->get('/matriculas');
        $response->assertStatus(200);
        $response->assertSee('No hay datos disponibles');
    }

    /** @test */
    public function a_user_can_view_all_tuitions()
    {
        $tuition = Tuition::factory()->create();
        $response = $this->get('/matriculas');
        $response->assertStatus(200);
        $response->assertSee([
            $tuition->id,
            $tuition->student->name,
            $tuition->degree->name,
        ]);
    }

    /** @test  */
    public function a_user_can_see_a_single_tuition()
    {
        $tuition = Tuition::factory()
            ->has(Subject::factory()->count(5))
            ->create();

        $response = $this->get('/matriculas/'.$tuition->id);
        $response->assertStatus(200);
        $response->assertSee([
            $tuition->student->fullName(),
            $tuition->degree->name,
            $tuition->subjects->first()->name
        ]);
    }

    /** @test  */
    public function a_user_can_navigate_to_create_tuition_page()
    {
        $response = $this->get('/matriculas/crear');
        $response->assertStatus(200);
        $response->assertSee([
            'Crear Matrícula',
            'Alumno',
            'Titulación'
        ]);
    }

    /** @test */
    public function a_user_can_create_a_tuition()
    {
        $tuition = Tuition::factory()->make();
        $this->post('matriculas', $tuition->toArray());
        $response = $this->get($tuition->path());
        $response->assertSee([
            $tuition->student->fullName(),
            $tuition->degree->name
            ]);
    }

    /** @test  */
    public function a_tuition_must_have_a_valid_degree()
    {
        $tuition = Tuition::factory()->make(
            [
                'degree_id' => 0
            ]
        );
        $response = $this->post('matriculas', $tuition->toArray());
        $response->assertSessionHasErrors('degree_id');
    }

    /** @test  */
    public function a_tuition_must_have_a_valid_student()
    {
        $tuition = Tuition::factory()->make(
            [
                'student_id' => 0
            ]
        );
        $response = $this->post('matriculas', $tuition->toArray());
        $response->assertSessionHasErrors('student_id');
    }

    /** @test  */
    public function a_student_must_have_a_unique_tuition_for_degree()
    {
        $tuition = Tuition::factory()->create();
        Tuition::factory()->make([
            'student_id ' => $tuition->student->id,
            'degree_id ' => $tuition->degree->id
        ]);
        $response = $this->post('matriculas', $tuition->toArray());
        $response->assertSessionHasErrors('degree_id');
    }

    /** @test  */
    public function a_user_can_navigate_to_edit_tuition_page()
    {
        $tuition = Tuition::factory()->create();
        $response = $this->get($tuition->path().'/editar');
        $response->assertStatus(200);
        $response->assertSee([
            $tuition->student->fullName(),
            $tuition->degree->name
        ]);
    }

    /** @test */
    public function a_user_can_edit_a_single_tuition()
    {
        $tuition = Tuition::factory()->create();
        $newDegree = Degree::factory()->create();
        $tuition->degree_id = $newDegree->id;

        $this->patch('matriculas/'.$tuition->id, $tuition->toArray() );
        $response = $this->get($tuition->path());
        $response->assertSee($newDegree->name);
    }

    /** @test */
    public function a_user_can_delete_a_single_tuition()
    {
        $tuition = Tuition::factory()->create();
        $this->delete('matriculas/'.$tuition->id);
        $this->assertDatabaseMissing('tuitions', [
            'id' => $tuition->id
        ]);
    }
}
