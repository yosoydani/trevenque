<?php

namespace Tests\Feature;

use App\Models\Degree;
use App\Models\Subject;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class DegreeTest extends TestCase
{

    use RefreshDatabase;

    /** @test */
    public function a_user_can_see_an_not_found_message_if_no_degrees(){
        $response = $this->get('/titulaciones');
        $response->assertStatus(200);
        $response->assertSee('No hay datos disponibles');
    }

    /** @test */
    public function a_user_can_view_all_degress()
    {
        $degree = Degree::factory()->create();
        $response = $this->get('/titulaciones');
        $response->assertStatus(200);
        $response->assertSee($degree->name);
    }

    /** @test  */
    public function a_user_can_see_a_single_degree()
    {
        $degree = Degree::factory()->create();
        $response = $this->get('/titulaciones/'.$degree->id);
        $response->assertStatus(200);
        $response->assertSee($degree->name);
    }

    /** @test  */
    public function a_user_can_navigate_to_create_degree_page()
    {
        $response = $this->get('/titulaciones/crear');
        $response->assertStatus(200);
        $response->assertSee([
            'Crear Titulación',
            'Nombre',
            'Enviar',
            'Reiniciar'
        ]);
    }

    /** @test */
    public function a_user_can_create_a_degree()
    {
        $degree = Degree::factory()->make();
        $this->post('titulaciones', $degree->toArray());
        $response = $this->get($degree->path());
        $response->assertSee($degree->name);
    }

    /** @test  */
    public function a_degree_must_have_a_name()
    {
        $degree = Degree::factory()->make(['name' => null]);

        $response = $this->post('titulaciones', $degree->toArray());
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function a_user_can_navigate_to_edit_degree_page()
    {
        $degree = Degree::factory()->create();
        $response = $this->get($degree->path().'/editar');
        $response->assertStatus(200);
        $response->assertSee($degree->name);
    }

    /** @test */
    public function a_user_can_edit_a_single_degree()
    {
        $degree = Degree::factory()->create();
        $newName = Str::random(25);
        $this->patch('titulaciones/'.$degree->id, ['name' => $newName] );
        $response = $this->get($degree->path());
        $response->assertSee($newName);
    }

    /** @test */
    public function a_user_can_delete_a_single_degree()
    {
        $degree = Degree::factory()->create();
        $this->delete('titulaciones/'.$degree->id);
        $this->assertDatabaseMissing('degrees', [
            'id' => $degree->id
        ]);
    }

    /** @test */
    public function if_a_degree_is_deleted_all_subjects_are_deleted() {
        $degree = Degree::factory()->create();
        $subject = Subject::factory(['degree_id' => $degree->id])->create();
        $this->delete('titulaciones/'.$degree->id);
        $this->assertDatabaseMissing('subjects', [
            'id' => $subject->id
        ]);
    }
}
