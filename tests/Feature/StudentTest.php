<?php

namespace Tests\Feature;

use App\Models\Student;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_see_an_not_found_message_if_no_students(){
        $response = $this->get('/alumnos');
        $response->assertStatus(200);
        $response->assertSee('No hay datos disponibles');
    }

    /** @test */
    public function a_user_can_view_all_students()
    {
        $student = Student::factory()->create();
        $response = $this->get('/alumnos');
        $response->assertStatus(200);
        $response->assertSee([
            $student->name,
            $student->surname,
            $student->birth_year,
        ]);
    }

    /** @test  */
    public function a_user_can_see_a_single_student()
    {
        $student = Student::factory()->create();
        $response = $this->get('/alumnos/'.$student->id);
        $response->assertStatus(200);
        $response->assertSee([
            $student->name,
            $student->surname,
            $student->birth_year,
        ]);
    }

    /** @test  */
    public function a_user_can_navigate_to_create_student_page()
    {
        $response = $this->get('/alumnos/crear');
        $response->assertStatus(200);
        $response->assertSee([
            'Crear Alumno',
            'Nombre',
            'Apellidos',
            'Año de nacimiento'
        ]);
    }

    /** @test */
    public function a_user_can_create_a_student()
    {
        $student = Student::factory()->make();
        $this->post('alumnos', $student->toArray());
        $response = $this->get($student->path());
        $response->assertSee($student->name);
    }

    /** @test  */
    public function a_student_must_have_a_name()
    {
        $student = Student::factory(['name' => null])->make();
        $response = $this->post('alumnos', $student->toArray());
        $response->assertSessionHasErrors('name');
    }

    /** @test  */
    public function a_student_must_have_a_surname()
    {
        $student = Student::factory(['surname' => null])->make();

        $response = $this->post('alumnos', $student->toArray());
        $response->assertSessionHasErrors('surname');
    }

    /** @test  */
    public function a_student_must_have_a_bird_year()
    {
        $student = Student::factory(['birth_year' => null])->make();

        $response = $this->post('alumnos', $student->toArray());
        $response->assertSessionHasErrors('birth_year');
    }

    /** @test  */
    public function a_student_must_have_a_bird_year_after_1900()
    {
        $subject = Student::factory()->make(['birth_year' => 1800]);

        $response = $this->post('alumnos', $subject->toArray());
        $response->assertSessionHasErrors('birth_year');
    }

    /** @test  */
    public function a_subject_must_have_a_year_before_current_year()
    {
        $subject = Student::factory()->make(['birth_year' => now()->addYear(1)->year]);

        $response = $this->post('alumnos', $subject->toArray());
        $response->assertSessionHasErrors('birth_year');
    }

    /** @test  */
    public function a_user_can_navigate_to_edit_student_page()
    {
        $student = Student::factory()->create();
        $response = $this->get($student->path().'/editar');
        $response->assertStatus(200);
        $response->assertSee([
            $student->name,
            $student->surname,
            $student->birth_year,
        ]);;
    }

    /** @test */
    public function a_user_can_edit_a_single_student()
    {
        $student = Student::factory()->create();
        $newName = Str::random(25);
        $student->name = $newName;

        $this->patch('alumnos/'.$student->id, $student->toArray() );
        $response = $this->get($student->path());
        $response->assertSee($newName);
    }

    /** @test */
    public function a_user_can_delete_a_single_student()
    {
        $student = Student::factory()->create();
        $this->delete('alumnos/'.$student->id);
        $this->assertDatabaseMissing('students', [
            'id' => $student->id
        ]);
    }
}
