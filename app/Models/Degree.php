<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Degree extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public function subjects() {
        return $this->hasMany(Subject::class);
    }

    public function tuitions() {
        return $this->hasMany(Tuition::class);
    }

    public function path() {
        return '/titulaciones/'. $this->id;
    }
}
