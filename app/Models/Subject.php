<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Subject extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function degree() {
        return $this->belongsTo(Degree::class);
    }

    public function tuitions() {
        return $this->belongsToMany(Tuition::class);
    }

    public function califications()
    {
        return $this->hasMany(Calification::class);
    }

    public function inscriptions(): int {
        return DB::table('subject_tuition')
            ->where('subject_id', $this->id)
            ->count();
    }

    public function isAvailable() : bool {
        return $this->inscriptions() < $this->max_students;
    }

    public function path() {
        return '/asignaturas/'. $this->id;
    }
}
