<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Reference\Fqsen;

class Student extends Model
{
    use HasFactory;

    protected $fillable = ['name','surname','birth_year'];

    public function tuitions() {
        return $this->hasMany(Tuition::class);
    }

    public function califications()
    {
        return $this->hasMany(Calification::class);
    }

    public function path() {
        return '/alumnos/'. $this->id;
    }

    public function fullName() {
        return $this->name .' ' . $this->surname;
    }
}
