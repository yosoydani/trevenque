<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tuition extends Model
{
    use HasFactory;

    protected $fillable = ['degree_id','student_id'];

    public function degree() {
        return $this->belongsTo(Degree::class);
    }

    public function student() {
        return $this->belongsTo(Student::class);
    }

    public function subjects() {
        return $this->belongsToMany(Subject::class);
    }

    public function path() {
        return '/matriculas/'. $this->id;
    }

    public function managePath() {
        return '/gestionar-matriculas/'. $this->id;
    }
}
