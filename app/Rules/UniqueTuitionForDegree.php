<?php

namespace App\Rules;

use App\Models\Tuition;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class UniqueTuitionForDegree implements Rule, DataAwareRule
{
    protected $data = [];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Tuition::where($attribute,$value)
            ->where('student_id',$this->data['student_id'])
            ->count() == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ya existe una matricula para ese estudiante en esa titulación.';
    }

    public function setData($data)
    {
        $this->data = $data;
    }
}
