<?php

namespace App\Rules;

use App\Models\Subject;
use Illuminate\Contracts\Validation\DataAwareRule;
use Illuminate\Contracts\Validation\Rule;

class SimilarSubjectInDegree implements Rule, DataAwareRule
{
    protected $data = [];

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        return Subject::where('name',$this->data['name'])
            ->where('credits',$this->data['credits'])
            ->where('max_students',$this->data['max_students'])
            ->where('academic_year',$this->data['academic_year'])
            ->where($attribute, $value)
            ->count() == 0;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Ya existe una asignatura similar en la títulación seleccionada';
    }

    public function setData($data)
    {
        $this->data = $data;
    }
}
