<?php

namespace App\Http\Controllers;

use App\Models\Degree;
use App\Models\Student;
use App\Models\Tuition;
use App\Rules\UniqueTuitionForDegree;
use Illuminate\Http\Request;

class TuitionController extends Controller
{
    private $currentSection = 'matriculas';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('tuitions.list', [
            'currentSection' => $this->currentSection,
            'tuitions' => Tuition::with('student', 'degree')->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectStudents = Student::all(['id', 'name','surname']);
        $selectDegrees = Degree::all(['id', 'name']);
        return view('tuitions.create', [
            'currentSection' => $this->currentSection,
            'selectDegrees' => $selectDegrees,
            'selectStudents' => $selectStudents
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'degree_id' => ['exists:degrees,id', new UniqueTuitionForDegree],
            'student_id' => 'exists:students,id'
        ]);

        Tuition::create($request->all());
        return redirect()->route('matriculas')->with('success', 'Matrícula creada correctamente ');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Tuition $tuition
     * @return \Illuminate\Http\Response
     */
    public function show(Tuition $tuition)
    {
        return view('tuitions.show', [
            'currentSection' => $this->currentSection,
            'tuition' => $tuition
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Tuition $tuition
     * @return \Illuminate\Http\Response
     */
    public function edit(Tuition $tuition)
    {
        $selectStudents = Student::all(['id', 'name','surname']);
        $selectDegrees = Degree::all(['id', 'name']);

        return view('tuitions.edit', [
            'currentSection' => $this->currentSection,
            'tuition' => $tuition,
            'selectDegrees' => $selectDegrees,
            'selectStudents' => $selectStudents
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Tuition $tuition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tuition $tuition)
    {
        $tuition->update($request->all());
        return redirect($tuition->path())->with('success','Matrícula actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Tuition $tuition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tuition $tuition)
    {
        $tuition->delete();
        return back()->with('success','Matrícula borrada correctamente');
    }
}
