<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    private $currentSection = 'alumnos';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.list', [
            'currentSection' => $this->currentSection,
            'students' => Student::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectStudent = Student::all(['id', 'name']);
        return view('students.create', [
            'currentSection' => $this->currentSection
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required','max:60'],
            'surname' => ['required','max:255'],
            'birth_year' =>['required','numeric','min:1900','max:'.now()->year]
        ]);

        Student::create($request->all());
        return redirect()->route('alumnos')->with('success', 'Alumno creado correctamente ');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('students.show', [
            'currentSection' => $this->currentSection,
            'student' => $student
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {

        return view('students.edit', [
            'currentSection' => $this->currentSection,
            'student' => $student
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $request->validate([
            'name' => ['required','max:60'],
            'surname' => ['required','max:255'],
            'birth_year' =>['required','numeric','min:1900','max:'.now()->year]
        ]);
        
        $student->update($request->all());

        return redirect($student->path())->with('success','Alumno actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();

        return back()->with('success','Alumno borrado correctamente');
    }
}
