<?php

namespace App\Http\Controllers;

use App\Models\Tuition;
use App\Rules\MaxIncriptions;
use Illuminate\Http\Request;

class ManageTuitionController extends Controller
{
    private $currentSection = 'titulaciones';

    /**
     * @return \Illuminate\Http\Response
     */
    public function manage(Tuition $tuition)
    {
        $availableSubjects = $tuition->degree->subjects;

        return view('manage-tuitions.manage',[
            'currentSection' => $this->currentSection,
            'tuition' => $tuition,
            'availableSubjects' => $availableSubjects
        ]);
    }

   public function addSubject(Request $request, Tuition $tuition)
   {
       $request->validate([
           'subject_id' => new MaxIncriptions()
       ]);
       $tuition->subjects()->attach($request->input('subject_id'));
       return back()->with('success','Asignatura añadida correctamente');
   }

   public function extractSubject(Request $request, Tuition $tuition)
   {
       $tuition->subjects()->detach($request->input('subject_id'));
       return back()->with('success','Asignatura eliminada correctamente');
   }
}
