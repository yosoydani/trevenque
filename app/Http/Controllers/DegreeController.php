<?php

namespace App\Http\Controllers;

use App\Models\Degree;
use Illuminate\Http\Request;

class DegreeController extends Controller
{
    private $currentSection = 'titulaciones';

    public function index()
    {
        return view('degrees.list',[
            'currentSection' => $this->currentSection,
            'degrees' =>  Degree::paginate(10)
        ]);
    }


    public function create()
    {
        return view('degrees.create',[
            'currentSection' => $this->currentSection,
        ]);
    }


    public function store(Request $request)
    {

        $request->validate([
            'name' => 'required|max:60'
        ]);

        Degree::create($request->all());

        return redirect()->route('titulaciones')->with('success','Titulación creada correctamente ');
    }


    public function show(Degree $degree)
    {
        return view('degrees.show',[
            'currentSection' => $this->currentSection,
            'degree' => $degree
        ]);
    }


    public function edit(Degree $degree)
    {
        return view('degrees.edit',[
            'currentSection' => $this->currentSection,
            'degree' => $degree
        ]);
    }


    public function update(Request $request, Degree $degree)
    {
        $request->validate([
            'name' => 'required|max:60'
        ]);

        $degree->update($request->all());

        return redirect($degree->path())->with('success','Titulación actualizada correctamente');
    }


    public function destroy(Degree $degree)
    {
        $degree->delete();
        return back()->with('success','Titulación borrada correctamente');


    }
}
