<?php

namespace App\Http\Controllers;

use App\Models\Calification;
use App\Models\Student;
use App\Models\Subject;
use Illuminate\Http\Request;

class CalificationController extends Controller
{
    private $currentSection = 'calificaciones';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('califications.list',[
            'currentSection' => $this->currentSection,
            'califications' =>  Calification::with(['student','subject'])->paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectStudents = Student::all(['id','name','surname']);
        $selectSubjects = Subject::all(['id','name']);
        return view('califications.create',[
            'currentSection' => $this->currentSection,
            'selectStudents' => $selectStudents,
            'selectSubjects' => $selectSubjects
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'student_id' => ['required','exists:students,id'],
            'subject_id' => ['required','exists:subjects,id'],
            'first_calification' => ['numeric','between:0,10'],
            'second_calification' => ['numeric','between:0,10'],
        ]);

        Calification::create($request->all());

        return redirect()->route('calificaciones')->with('success','Calificación creada correctamente ');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Calification  $calification
     * @return \Illuminate\Http\Response
     */
    public function show(Calification $calification)
    {
        return view('califications.show',[
            'currentSection' => $this->currentSection,
            'calification' => $calification
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Calification  $calification
     * @return \Illuminate\Http\Response
     */
    public function edit(Calification $calification)
    {
        $selectStudents = Student::all(['id','name','surname']);
        $selectSubjects = Subject::all(['id','name']);

        return view('califications.edit',[
            'currentSection' => $this->currentSection,
            'selectStudents' => $selectStudents,
            'selectSubjects' => $selectSubjects,
            'calification' => $calification
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Calification  $calification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Calification $calification)
    {
        $request->validate([
            'student_id' => ['required','exists:students,id'],
            'subject_id' => ['required','exists:subjects,id'],
            'first_calification' => ['numeric','between:0,10'],
            'second_calification' => ['numeric','between:0,10'],
        ]);

        $calification->update($request->all());

        return redirect($calification->path())->with('success','Calificación actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Calification  $calification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Calification $calification)
    {
        $calification->delete();
        return back()->with('success','Calificación borrada correctamente');
    }
}
