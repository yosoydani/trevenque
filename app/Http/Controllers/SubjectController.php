<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSubjectRequest;
use App\Http\Requests\UpdateSubjectRequest;
use App\Models\Degree;
use App\Models\Subject;

class SubjectController extends Controller
{
    private $currentSection = 'asignaturas';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subjects.list', [
            'currentSection' => $this->currentSection,
            'subjects' => Subject::paginate(10)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $selectDegrees = Degree::all(['id', 'name']);
        return view('subjects.create', [
            'currentSection' => $this->currentSection,
            'selectDegrees' => $selectDegrees
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreSubjectRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubjectRequest $request)
    {
        Subject::create($request->all());
        return redirect()->route('asignaturas')->with('success', 'Asignatura creada correctamente ');

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return view('subjects.show', [
            'currentSection' => $this->currentSection,
            'subject' => $subject
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function edit(Subject $subject)
    {
        $selectDegrees = Degree::all(['id', 'name']);

        return view('subjects.edit', [
            'currentSection' => $this->currentSection,
            'selectDegrees' => $selectDegrees,
            'subject' => $subject
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateSubjectRequest $request
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubjectRequest $request, Subject $subject)
    {
        $subject->update($request->all());

        return redirect($subject->path())->with('success','Asignatura actualizada correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Subject $subject
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject)
    {
        $subject->delete();

        return back()->with('success','Asignatura borrada correctamente');
    }
}
