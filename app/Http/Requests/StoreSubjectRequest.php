<?php

namespace App\Http\Requests;

use App\Rules\SimilarSubjectInDegree;
use Illuminate\Foundation\Http\FormRequest;

class StoreSubjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'max:60'],
            'credits' => ['required', 'numeric', 'min:1'],
            'max_students' => ['required', 'numeric', 'min:1'],
            'academic_year' => ['required', 'numeric', 'between:2020,2155'],
            'degree_id' => ['exists:degrees,id', new SimilarSubjectInDegree]
        ];
    }
}
