<?php

use App\Http\Controllers\CalificationController;
use App\Http\Controllers\DegreeController;
use App\Http\Controllers\ManageTuitionController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\SubjectController;
use App\Http\Controllers\TuitionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome',['currentSection' => 'inicio']);
});

// titulaciones
Route::prefix('titulaciones')->group(function () {
    Route::get('/',[DegreeController::class,'index'])->name('titulaciones');
    Route::post('/',[DegreeController::class,'store']);
    Route::get('/crear',[DegreeController::class,'create']);
    Route::get('/{degree}',[DegreeController::class,'show']);
    Route::patch('/{degree}',[DegreeController::class,'update']);
    Route::delete('/{degree}',[DegreeController::class,'destroy']);
    Route::get('/{degree}/editar',[DegreeController::class,'edit']);
});

// Asignaturas
Route::prefix('asignaturas')->group(function () {
    Route::get('/',[SubjectController::class,'index'])->name('asignaturas');
    Route::post('/',[SubjectController::class,'store']);
    Route::get('/crear',[SubjectController::class,'create']);
    Route::get('/{subject}',[SubjectController::class,'show']);
    Route::patch('/{subject}',[SubjectController::class,'update']);
    Route::delete('/{subject}',[SubjectController::class,'destroy']);
    Route::get('/{subject}/editar',[SubjectController::class,'edit']);
});


// alumnos
Route::prefix('alumnos')->group(function () {
    Route::get('/',[StudentController::class,'index'])->name('alumnos');
    Route::post('/',[StudentController::class,'store']);
    Route::get('/crear',[StudentController::class,'create']);
    Route::get('/{student}',[StudentController::class,'show']);
    Route::patch('/{student}',[StudentController::class,'update']);
    Route::delete('/{student}',[StudentController::class,'destroy']);
    Route::get('/{student}/editar',[StudentController::class,'edit']);
});

// matriculas
Route::prefix('matriculas')->group(function () {
    Route::get('/',[TuitionController::class,'index'])->name('matriculas');
    Route::post('/',[TuitionController::class,'store']);
    Route::get('/crear',[TuitionController::class,'create']);
    Route::get('/{tuition}',[TuitionController::class,'show']);
    Route::patch('/{tuition}',[TuitionController::class,'update']);
    Route::delete('/{tuition}',[TuitionController::class,'destroy']);
    Route::get('/{tuition}/editar',[TuitionController::class,'edit']);
});

// gestionar
Route::prefix('gestionar-matriculas')->group(function (){
    Route::get('/{tuition}',[ManageTuitionController::class,'manage']);
    Route::post('/{tuition}',[ManageTuitionController::class,'addSubject']);
    Route::delete('/{tuition}',[ManageTuitionController::class,'extractSubject']);

});

// calificaciones
Route::prefix('calificaciones')->group(function () {
    Route::get('/',[CalificationController::class,'index'])->name('calificaciones');
    Route::post('/',[CalificationController::class,'store']);
    Route::get('/crear',[CalificationController::class,'create']);
    Route::get('/{calification}',[CalificationController::class,'show']);
    Route::patch('/{calification}',[CalificationController::class,'update']);
    Route::delete('/{calification}',[CalificationController::class,'destroy']);
    Route::get('/{calification}/editar',[CalificationController::class,'edit']);
});

