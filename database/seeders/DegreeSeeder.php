<?php

namespace Database\Seeders;

use App\Models\Degree;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DegreeSeeder extends Seeder
{
    use WithoutModelEvents;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('degrees')->insert([
            'id' => 1,
            'name' => 'Ing. informática',
            'created_at' => now()
        ]);

        DB::table('degrees')->insert([
            'id' => 2,
            'name' => 'Empresariales',
            'created_at' => now()
        ]);

      //  Degree::factory()->count(100)->create();
    }
}
