<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'name' => 'Fundamentos físicos y tecnológicos de la informática',
            'degree_id' => 1,
            'created_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'Lógica',
            'degree_id' => 1,
            'created_at' => now()

        ]);
        DB::table('subjects')->insert([
            'name' => 'Programación I',
            'degree_id' => 1,
            'created_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'Contabilidad',
            'degree_id' => 2,
            'created_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'Derecho',
            'degree_id' => 2,
            'created_at' => now()
        ]);
        DB::table('subjects')->insert([
            'name' => 'Econometria',
            'degree_id' => 2,
            'created_at' => now()
        ]);

    }
}
