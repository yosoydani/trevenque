<?php

namespace Database\Seeders;

use App\Models\Degree;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Tuition;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class TuitionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tuition::factory()
            ->has(Subject::factory()->count(5))
            ->create([
            'degree_id' => Degree::first(),
            'student_id' => Student::first(),
        ]);
    }
}
