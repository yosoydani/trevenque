<?php

namespace Database\Factories;

use App\Models\Degree;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Subject>
 */
class SubjectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->text(22),
            'credits' => $this->faker->numberBetween(0,6),
            'max_students' => $this->faker->numberBetween(10,50),
            'academic_year' => now()->year,
            'degree_id' => Degree::factory()
        ];
    }
}
