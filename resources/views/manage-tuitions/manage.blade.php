@extends('shared.layout')
@section('content')
    <div class="row">
        @include('../shared/feedback-success')
        @include('../shared/feedback-error')
        <div class="col">
            <h1 class="text-center">{{$tuition->student->fullName()}}</h1>
        </div>
    </div>
    <hr>
    <div class="row">
        <form method="POST" action="{{$tuition->managePath()}}">
            @csrf
            <div class="mb-3">
                <label for="subject_id" class="form-label">Asignatura *</label>
                <select class="form-select @error('subject_id') is-invalid  @enderror" id="subject_id" required
                        name="subject_id">
                    <option value="0">Selecciona una asignatura</option>
                    @foreach($availableSubjects as $subject)
                        <option
                            value="{{$subject->id}}" {{ old('subject_id')  == $subject->id ? 'selected' : ''}}>{{$subject->name}}
                        </option>
                    @endforeach
                </select>
            </div>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Añadir</button>
        </form>
    </div>
    <div class="row">
        <div class="card">
            <div class="card-header mt-3">
                <h2 class="text-center">{{ $tuition->degree->name }}</h2></div>
            <div class="card-body">
                <h3 class="text-center">Asignaturas</h3>
                @if ($tuition->subjects->count() === 0)
                    <h2>No hay datos disponibles</h2>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Créditos</th>
                            <th scope="col">Alumnos máximos</th>
                            <th scope="col">Curso Académico</th>
                            <th scope="col">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tuition->subjects as $subject)
                            <tr>
                                <th scope="row">{{$subject->id}}</th>
                                <td>{{$subject->name}}</td>
                                <td>{{$subject->credits}}</td>
                                <td>{{$subject->max_students}}</td>
                                <td>{{$subject->academic_year}}</td>
                                <td>
                                    <form style="margin: 0; padding: 0;" method="POST" action="{{$tuition->managePath()}}">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="subject_id" value="{{$subject->id}}">
                                        <button class="btn btn-danger">Eliminar</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="/matriculas">
                    <button type="button" class="btn btn-primary">Volver</button>
                </a>
            </div>
        </div>
    </div>
@endsection
