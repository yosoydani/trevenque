@extends('shared.layout')
@section('content')
    <h1>Sistema de gestión de matriculas</h1>
    <div>
        Selecciones una opción.
    </div>
    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
            <a class="nav-link" href="/titulaciones">Gestionar Titulaciones</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/matriculas">Gestionar Matrículas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/asignaturas">Gestionar Asignaturas</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/alumnos">Gestionar Alumnos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/calificaciones">Gestionar Calificaciones</a>
        </li>
    </ul>
@endsection
