@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Titulaciones</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="titulaciones/crear" class="btn btn-primary w-100">Crear</a>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-success')

    @if($degrees->count() <= 0)
        <h2>No hay datos disponibles</h2>
    @else
        <table class="table">
            <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th class="text-center" scope="col">Acciones</th>
        </tr>
        </thead>
            <tbody>
            @foreach($degrees as $degree)
                <tr>
                <th scope="row">{{ $degree->id }}</th>
                <td>{{$degree->name}}</td>
                <td class="row">
                    <div class="col-4">
                        <a class="btn btn-success" href="{{$degree->path()}}">
                        <span>
                            Ver
                        </span>
                        </a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-warning" href="{{$degree->path()}}/editar">
                            <span>Editar</span>
                        </a>
                    </div>
                    <div class="col-4">
                        <form style="margin: 0; padding: 0;"  method="POST" action="/titulaciones/{{$degree->id}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Borrar</button>
                        </form>
                    </div>
                </td>
            </tr>
                @endforeach
        </tbody>
        </table>
        {{ $degrees->links() }}
    @endif
    <div class="row mt-3">
        <div class="col">
            <a href="/"><button type="button" class="btn btn-primary">Volver</button>
            </a>
        </div>
    </div>
@endsection
