@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Editar Titulación</h1>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-error')
    <div class="row">
        <form method="POST" action="/titulaciones/{{$degree->id}}" >
            @csrf
            @method('PATCH')
            <div class="mb-3">
                <label for="name" class="form-label">Nombre*</label>
                <input type="input" id="name" name="name" required value="{{ old('name',$degree->name)}}" class="form-control @error('name') is-invalid @enderror" >
            </div>
            <a href="/titulaciones" class="btn btn-primary">Volver</a>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@endsection
