@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Crear Alumno</h1>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-error')

    <div class="row">
        <form method="POST" action="/alumnos" >
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label">Nombre*</label>
                <input type="input" id="name" placeholder="Daniel" name="name" required value="{{ old('name') }}" class="form-control @error('name') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="surname" class="form-label">Apellidos*</label>
                <input type="input" id="surname" placeholder="Sánchez Cuadrado" name="surname" required value="{{ old('surname') }}" class="form-control @error('surname') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="birth_year" class="form-label">Año de nacimiento*</label>
                <input type="number" placeholder="1980" min="1900" max="{{now()->year}} "id="birth_year" name="birth_year" required value="{{ old('birth_year') }}" class="form-control @error('birth_year') is-invalid  @enderror" >
            </div>
            <a href="/titulaciones" class="btn btn-primary">Volver</a>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@endsection
