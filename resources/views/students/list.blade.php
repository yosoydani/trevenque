@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Alumnos</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="alumnos/crear" class="btn btn-primary w-100">Crear</a>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-success')

    @if($students->count() <= 0)
        <h2>No hay datos disponibles</h2>
    @else
        <table class="table">
            <thead>
            <tr>
            <th scope="col">Id</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Año nacimiento</th>
            <th class="text-center" scope="col">Acciones</th>
        </tr>
        </thead>
            <tbody>
            @foreach($students as $student)
                <tr>
                <th scope="row">{{ $student->id }}</th>
                <td>{{$student->name}}</td>
                <td>{{$student->surname}}</td>
                <td>{{$student->birth_year}}</td>
                <td class="row">
                    <div class="col-4">
                        <a class="btn btn-success" href="{{$student->path()}}">
                        <span>
                            Ver
                        </span>
                        </a>
                    </div>
                    <div class="col-4">
                        <a class="btn btn-warning" href="{{$student->path()}}/editar">
                            <span>Editar</span>
                        </a>
                    </div>
                    <div class="col-4">
                        <form style="margin: 0; padding: 0;"  method="POST" action="/alumnos/{{$student->id}}">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger">Borrar</button>
                        </form>
                    </div>
                </td>
            </tr>
                @endforeach
        </tbody>
        </table>
        {{ $students->links() }}
    @endif
    <div class="row mt-3">
        <div class="col">
            <a href="/"><button type="button" class="btn btn-primary">Volver</button>
            </a>
        </div>
    </div>
@endsection
