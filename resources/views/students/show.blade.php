@extends('shared.layout')
@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">{{$student->fullName()}}</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="/alumnos/{{$student->id}}/editar" class="btn btn-primary w-100">Editar</a>
        </div>
    </div>
    <hr>
    <div class="row">
        @include('../shared/feedback-success')
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Año de nacimiento</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <th scope="row">{{$student->id}}</th>
                        <td>{{$student->name}}</td>
                        <td>{{$student->surname}}</td>
                        <td>{{$student->birth_year}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="/alumnos"><button type="button" class="btn btn-primary">Volver</button>
                </a>
            </div>
        </div>
    </div>
@endsection
