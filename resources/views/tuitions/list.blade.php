@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Matrículas</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="matriculas/crear" class="btn btn-primary w-100">Crear</a>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-success')

    @if($tuitions->count() <= 0)
        <h2>No hay datos disponibles</h2>
    @else
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Alumno</th>
                <th scope="col">Titulación</th>
                <th class="text-center" scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tuitions as $tuition)
                <tr>
                    <th scope="row">{{ $tuition->id }}</th>
                    <td>{{$tuition->student->fullname()}}</td>
                    <td>{{$tuition->degree->name}}</td>
                    <td class="row">
                        <div class="col-3">
                            <a class="btn btn-success" href="{{$tuition->path()}}">
                        <span>
                            Ver
                        </span>
                            </a>
                        </div>
                        <div class="col-3">
                            <a class="btn btn-warning" href="{{$tuition->path()}}/editar">
                                <span>Editar</span>
                            </a>
                        </div>
                        <div class="col-3">
                            <a class="btn btn-secondary" href="{{$tuition->managePath()}}">
                                <span>Gestionar</span>
                            </a>
                        </div>
                        <div class="col-3">
                            <form style="margin: 0; padding: 0;" method="POST" action="/matriculas/{{$tuition->id}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $tuitions->links() }}
    @endif
    <div class="row mt-3">
        <div class="col">
            <a href="/">
                <button type="button" class="btn btn-primary">Volver</button>
            </a>
        </div>
    </div>
@endsection
