@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Crear Matrícula</h1>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-error')

    <div class="row">
        <form method="POST" action="/matriculas">
            @csrf
            <div class="mb-3">
                <label for="student_id" class="form-label">Alumno *</label>
                <select class="form-select @error('student_id') is-invalid  @enderror" id="student_id" required
                        name="student_id">
                    <option value="0">Selecciona un alumno</option>
                    @foreach($selectStudents as $student)
                        <option
                            value="{{$student->id}}" {{ old('student_id')  == $student->id ? 'selected' : ''}}>{{$student->fullName()}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="degree_id" class="form-label">Titulación *</label>
                <select class="form-select @error('degree_id') is-invalid  @enderror" id="degree_id" required
                        name="degree_id">
                    <option value="0">Selecciona una titulación</option>
                    @foreach($selectDegrees as $degree)
                        <option
                            value="{{$degree->id}}" {{ old('degree_id')  == $degree->id ? 'selected' : ''}}>{{$degree->name}}</option>
                    @endforeach
                </select>
            </div>
            <a href="/matriculas" class="btn btn-primary">Volver</a>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@endsection
