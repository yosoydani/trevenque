@extends('shared.layout')
@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">{{$tuition->student->fullName()}}</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="/matriculas/{{$tuition->id}}/editar" class="btn btn-primary w-100">Editar</a>
        </div>
    </div>
    <hr>
    <div class="row">

        @include('../shared/feedback-success')
        <div class="card">
            <div class="card-header mt-3">
                <h2 class="text-center">{{ $tuition->degree->name }}</h2></div>
            <div class="card-body">
                <h3 class="text-center">Asignaturas</h3>
                @if ($tuition->subjects->count() === 0)
                    <h2>No hay datos disponibles</h2>
                @else
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Créditos</th>
                            <th scope="col">Alumnos máximos</th>
                            <th scope="col">Curso Académico</th>
{{--                            <th scope="col">Acciones</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($tuition->subjects as $subject)
                            <tr>
                                <th scope="row">{{$subject->id}}</th>
                                <td>{{$subject->name}}</td>
                                <td>{{$subject->credits}}</td>
                                <td>{{$subject->max_students}}</td>
                                <td>{{$subject->academic_year}}</td>
{{--                                <td>--}}
{{--                                    <a class="btn btn-success" href="/asignaturas/{{$subject->id}}">--}}
{{--                                                    <span>--}}
{{--                                                        Ver--}}
{{--                                                    </span>--}}
{{--                                    </a>--}}
{{--                                    <a class="btn btn-warning" href="/asignaturas/{{$subject->id}}/editar">--}}
{{--                                        <span>Editar</span>--}}
{{--                                    </a>--}}
{{--                                </td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="/matriculas">
                    <button type="button" class="btn btn-primary">Volver</button>
                </a>
            </div>
        </div>
    </div>
@endsection
