@extends('shared.layout')
@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">{{$calification->student->fullName()}}</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="/calificaciones/{{$calification->id}}/editar" class="btn btn-primary w-100">Editar</a>
        </div>
    </div>
    <hr>
    <div class="row">

        @include('../shared/feedback-success')
        <div class="card">
            <div class="card-header mt-3">
                <h2 class="text-center">{{ $calification->subject->name }}</h2></div>
            <div class="card-body">
                <h3 class="text-center">Asignaturas</h3>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Primera convocatoria</th>
                        <th scope="col">Segunda convocatoria</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">{{$calification->id}}</th>
                        <td>{{$calification->first_calification}}</td>
                        <td>{{$calification->second_calification}}</td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="/calificaciones">
                    <button type="button" class="btn btn-primary">Volver</button>
                </a>
            </div>
        </div>
    </div>
@endsection
