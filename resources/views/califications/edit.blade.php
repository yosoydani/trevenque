@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Editar Calificación</h1>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-error')

    <div class="row">
        <form method="POST" action="{{$calification->path()}}">
            @csrf
            @method('PATCH')
            <div class="mb-3">
                <label for="student_id" class="form-label">Alumno *</label>
                <select class="form-select @error('student_id') is-invalid  @enderror" id="student_id" required
                        name="student_id">
                    <option value="0">Selecciona un alumno</option>
                    @foreach($selectStudents as $student)
                        <option
                            value="{{$student->id}}" {{ old('student_id',$student->id)  == $student->id ? 'selected' : ''}}>{{$student->fullName()}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="subject_id" class="form-label">Titulación *</label>
                <select class="form-select @error('subject_id') is-invalid  @enderror" id="subject_id" required
                        name="subject_id">
                    <option value="0">Selecciona una titulación</option>
                    @foreach($selectSubjects as $subject)
                        <option
                            value="{{$subject->id}}" {{ old('subject_id',$subject->id)  == $subject->id ? 'selected' : ''}}>{{$subject->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3">
                <label for="first_calification" class="form-label">Primera Convocatoria *</label>
                <input type="number" min="0" max="10" step="any" id="first_calification" required placeholder="0"
                       name="first_calification"  value="{{ old('first_calification', $calification->first_calification) }}" class="form-control @error('first_calification') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="second_calification" class="form-label">Segunda Convocatoria *</label>
                <input type="number" min="0" max='10' step="any" id="second_calification" required placeholder="0"
                       name="second_calification"  value="{{ old('second_calification',$calification->second_calification,) }}" class="form-control @error('second_calification') is-invalid  @enderror" >
            </div>
            <a href="/calificaciones" class="btn btn-primary">Volver</a>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@endsection
