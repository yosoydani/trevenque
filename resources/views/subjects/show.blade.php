@extends('shared.layout')
@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">{{$subject->name}}</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="/titulaciones/{{$subject->id}}/editar" class="btn btn-primary w-100">Editar</a>
        </div>
    </div>
    <hr>
    <div class="row">
        @include('../shared/feedback-success')
        <div class="card">
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Titulación</th>
                        <th scope="col">Créditos</th>
                        <th scope="col">Máx. alumnos</th>
                        <th scope="col">Curso Académico</th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <th scope="row">{{$subject->id}}</th>
                        <td>{{$subject->name}}</td>
                        <td>{{$subject->degree->name}}</td>
                        <td>{{$subject->credits}}</td>
                        <td>{{$subject->max_students}}</td>
                        <td>{{$subject->academic_year}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col">
                <a href="/asignaturas"><button type="button" class="btn btn-primary">Volver</button>
                </a>
            </div>
        </div>
    </div>
@endsection
