@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Crear Asignatura</h1>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-error')

    <div class="row">
        <form method="POST" action="/asignaturas" >
            @csrf
            <div class="mb-3">
                <label for="name" class="form-label" >Nombre *</label>
                <input type="input" id="name" name="name"  required placeholder="Programación" value="{{ old('name') }}" class="form-control @error('name') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="credits" class="form-label">Créditos *</label>
                <input type="number" min="1" id="credits" required placeholder="6" name="credits"  value="{{ old('credits') }}" class="form-control @error('credits') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="max_students" class="form-label">Máx. alumnos *</label>
                <input type="number" min="1" id="max_students" required placeholder="50" name="max_students"  value="{{ old('max_students') }}" class="form-control @error('max_students') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="academic_year" class="form-label">Año académico *</label>
                <input type="number" min="2020" max="2155" id="academic_year" required placeholder="{{now()->year}}"name="academic_year"  value="{{ old('academic_year') }}" class="form-control @error('academic_year') is-invalid  @enderror" >
            </div>
            <div class="mb-3">
                <label for="degree_id" class="form-label">Titulación *</label>
                <select class="form-select @error('degree_id') is-invalid  @enderror" id="degree_id" required name="degree_id">
                    <option value="0">Selecciona una titulación</option>
                    @foreach($selectDegrees as $degree)
                    <option value="{{$degree->id}}" {{ old('degree_id')  == $degree->id ? 'selected' : ''}}>{{$degree->name}}</option>
                    @endforeach
                </select>
            </div>
            <a href="/asignaturas" class="btn btn-primary">Volver</a>
            <button type="reset" class="btn btn-danger">Reiniciar</button>
            <button type="submit" class="btn btn-success">Enviar</button>
        </form>
    </div>
@endsection
