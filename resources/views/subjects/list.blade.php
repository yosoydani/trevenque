@extends('shared.layout')

@section('content')
    <div class="row">
        <div class="col-md-10 col-8">
            <h1 class="text-center">Asignaturas</h1>
        </div>
        <div class="col-md-2 col-4">
            <a href="asignaturas/crear" class="btn btn-primary w-100">Crear</a>
        </div>
    </div>
    <hr>
    @include('../shared/feedback-success')

    @if($subjects->count() <= 0)
        <h2>No hay datos disponibles</h2>
    @else
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Titulación</th>
                <th scope="col">Créditos</th>
                <th scope="col">Máx. alumnos</th>
                <th scope="col">Año</th>
                <th class="text-center" scope="col">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($subjects as $subject)
                <tr>
                    <th scope="row">{{ $subject->id }}</th>
                    <td>{{$subject->name}}</td>
                    <td>{{$subject->degree->name}}</td>
                    <td>{{$subject->credits}}</td>
                    <td>{{$subject->max_students}}</td>
                    <td>{{$subject->academic_year}}</td>

                    <td class="row">
                        <div class="col-4">
                            <a class="btn btn-success" href="{{$subject->path()}}">
                        <span>
                            Ver
                        </span>
                            </a>
                        </div>
                        <div class="col-4">
                            <a class="btn btn-warning" href="{{$subject->path()}}/editar">
                                <span>Editar</span>
                            </a>
                        </div>
                        <div class="col-4">
                            <form style="margin: 0; padding: 0;" method="POST" action="/asignaturas/{{$subject->id}}">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger">Borrar</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $subjects->links() }}
    @endif
    <div class="row mt-3">
        <div class="col">
            <a href="/">
                <button type="button" class="btn btn-primary">Volver</button>
            </a>
        </div>
    </div>
@endsection
