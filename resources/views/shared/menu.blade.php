<div class="row">

    <nav class="navbar navbar-expand-lg navbar-light bg-light static-top">
        <div class="container-fluid">
            <a class="navbar-brand" href="/"><img height="55px" width="200px" src="{{ asset('images/ugr.svg') }}" alt="logo universidad granada"></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'inicio' ? 'active' : '' }}"  href="/">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'titulaciones' ? 'active' : '' }}" href="/titulaciones">Titulaciones</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'matriculas' ? 'active' : '' }}" href="/matriculas">Matrículas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'asignaturas' ? 'active' : '' }}" href="/asignaturas">Asignaturas</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'alumnos' ? 'active' : '' }}" href="/alumnos">Alumnos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link {{ $currentSection == 'calificaciones' ? 'active' : '' }}" href="/calificaciones">Calificaciones</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<hr>
